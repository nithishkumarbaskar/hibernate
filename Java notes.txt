Static keyword:
	static keyword is mainly used for memory management.it can be used with class,variable,method.
 The static variable gets memory only once in the class area at the time of class loading. we can access static 
without object.for static class we need nested class. i.e class within a class.


String:
	It is a child of CharSequence interface. it is immutable. i.e it can't be changed.
String Buffer:
	It is a child of CharSequence interface
	It is mutable
	It is synchronized/THREAD SAFE.[multi threads can't access it simultaneously]
	less efficient
	It can be created with new keyword only.
String Builder:
	It is a child of CharSequence interface
	It is mutable
	It is non-synchronized/NOT THREAD SAFE.[multi threads can access it simultaneously]
	more efficient
	It can be created with new keyword only.

The main difference between class and interface is that class can be instantiated and constructor can be
 used, but in interface we can't do that.

						


							JAVA 8

Default methods:
	Methods which are defined inside the interface and tagged with default are known as 
default methods.default methods can have body content in them unlike abstract.
Static method in interfaces:
	we can define static method inside interface.Static methods are used to define utility methods.
Functional interfaces:
	An Interface that contains exactly one abstract method is known as functional interface. 
	It can have any number of default, static methods but can contain only one abstract method. 
	It can also declare methods of object class.
Lambda expressions:
	It is used to reduce the number of lines of code. 
	It is used for implementation of functional interfaces
	the syntax is ["->"]
Method references:
	Method reference is used to refer method of functional interface. 
	It is compact and easy form of lambda expression. 
	Each time when you are using lambda expression to just referring a method, you can replace your lambda expression with method reference
Stream API:
	It is a feature which is used instead of traditional conditions(i.e looping,conditions). Using stream, you can process data in a declarative way similar to SQL statements.it is lazy loader.Ut does not store elements.
it consist of two types
1) intermediate -->it includes filter()[It uses Predicate(i.e with only one argument{boolean type})],
map()[Function(i.e one argument with one return type) ],flatmap(),distinct(),sorted(),peek(),limit(),skip()
2) Terminal     -->It includes toArray(),collect(),count(),reduce(),forEach()[It uses Consumer],ForEachOrdered(),min(),max(),anymatch(),allmatch(),
noneMatch(),findAny(),findFirst()
A java file may contain as many intermediate streams, but there should be only one Terminal stream

Optional class:
	
collectors class:
	
Foreach method:
	It is the enhanced version of for loop which uses consumer type functional interface(i.e with one argument and no return type)
Date API:
	it helps to get the current date. we use local date
